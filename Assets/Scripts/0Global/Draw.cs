﻿using UnityEngine;
using System.Collections;

public class Draw : MonoBehaviour
{
    private pauseManager pManager;

    GameObject g;
    void Start()
    {
        this.pManager = pauseManager.getInstance();

        switch (Application.loadedLevelName)
        {
            case "Game":
                this.drawGame(this.g);
                break;
        }

    }

    void Update()
    {
        if(Input.GetKeyUp(KeyCode.P) && !this.pManager.getPause())
            this.pManager.setPause(true);
        else if (Input.GetKeyUp(KeyCode.P) && this.pManager.getPause())
            this.pManager.setPause(false);
    }

    void drawGame(GameObject g)
    {
        //Backgrounds
        g = new GameObject("a0", typeof(backgroundG));
        for (int i = 1; i < 5; i++) { g = new GameObject("a" + i, typeof(backgroundG)); g = new GameObject("b" + i, typeof(backgroundG)); }

        g = new GameObject("player", typeof(Player));
    }
}
