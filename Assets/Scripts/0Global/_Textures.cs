﻿using UnityEngine;
using System.Collections;

public class _Textures
{

    private static _Textures instance = null;

    public static _Textures getInstance()
    {
        if (instance == null)
            instance = new _Textures();
        return instance;
    }

    //Game
    public Sprite[] backgroundGame, player, enemys, shot, cocktail, enemys2, collictible;
    public Sprite obstacles;

    public _Textures()
    {
        this.T_Game();
    }

    private void T_Game()
    {
        this.backgroundGame = new Sprite[5];
        this.player = new Sprite[12];
        this.shot = new Sprite[4];
        this.enemys = new Sprite[11];
        this.enemys2 = new Sprite[11];
        this.cocktail = new Sprite[2];
        this.collictible = new Sprite[2];

        for (int i = 0; i < 5; i++)
            this.backgroundGame[i] = Resources.Load("Game/Background/b" + i, typeof(Sprite)) as Sprite;

        for (int i = 0; i < 12; i++)
            this.player[i] = Resources.Load("Game/Player/p" + (i + 1), typeof(Sprite)) as Sprite;

        for (int i = 0; i < 4; i++)
            this.shot[i] = Resources.Load("Game/Shot/b" + (i + 1), typeof(Sprite)) as Sprite;

        for (int i = 0; i < 11; i++)
        {
            this.enemys[i] = Resources.Load("Game/Enemys/e" + (i + 1), typeof(Sprite)) as Sprite;
            this.enemys2[i] = Resources.Load("Game/Enemys/g" + (i + 1), typeof(Sprite)) as Sprite;
        }

        for (int i = 0; i < 2; i++)
            this.cocktail[i] = Resources.Load("Game/Obstacles/m" + (i + 1), typeof(Sprite)) as Sprite;

        this.collictible[0] = Resources.Load("Game/Collectible/water", typeof(Sprite)) as Sprite;
        this.collictible[1] = Resources.Load("Game/Collectible/ammo", typeof(Sprite)) as Sprite;
    }
}
