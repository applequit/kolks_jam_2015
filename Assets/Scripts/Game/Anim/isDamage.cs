﻿using UnityEngine;
using System.Collections;

public class isDamage : MonoBehaviour {

    public bool endAnim;
	void Start () {
        this.gameObject.AddComponent<Animator>();

        this.gameObject.GetComponent<Animator>().runtimeAnimatorController = 
            (RuntimeAnimatorController)RuntimeAnimatorController.Instantiate(Resources.Load("Anim/Damage/player"));

        this.gameObject.GetComponent<Animator>().SetBool("damageOn", false);

	}
	
	void Update () {
	    if(this.endAnim)
            this.gameObject.GetComponent<Animator>().SetBool("damageOn", false);
	}
}
