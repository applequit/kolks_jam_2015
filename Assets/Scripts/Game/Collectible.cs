﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour
{
    private pauseManager pManager;
    private _Textures textures;

    public Vector2 position;
    public int skin;

    void Start()
    {

        this.pManager = pauseManager.getInstance();
        this.textures = _Textures.getInstance();

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<Rigidbody2D>();
        this.gameObject.AddComponent<Animator>();

        this.gameObject.GetComponent<Animator>().runtimeAnimatorController =
            (RuntimeAnimatorController)RuntimeAnimatorController.Instantiate(Resources.Load("Anim/Collictible/ammo"));

        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.textures.collictible[this.skin];
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 6;

        this.GetComponent<Rigidbody2D>().fixedAngle = true;
        this.GetComponent<Rigidbody2D>().gravityScale = 0;

        this.transform.position = this.position;

    }

    void Update()
    {
    }

    void FixedUpdate()
    {
        this.addPolygon();

        if (!this.pManager.getPause())
        {
            if (this.transform.position.x + this.textures.collictible[this.skin].rect.width / 100 < -4 || this.transform.position.y + this.textures.collictible[this.skin].rect.height / 100 < -3)
                Destroy(this.gameObject);

            transform.Translate(-.054f, 0, 0);
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.name.Equals("player"))
        {
            if (this.gameObject.name.StartsWith("a"))
            {
                c.gameObject.GetComponent<SoundFX>().liberty = true;
                c.gameObject.GetComponent<SoundFX>().sublabel = "reload";
            }
            Destroy(this.gameObject);
        }
    }

    void addPolygon()
    {
        if (!this.gameObject.GetComponent<PolygonCollider2D>())
            this.gameObject.AddComponent<PolygonCollider2D>();

        this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
    }

}
