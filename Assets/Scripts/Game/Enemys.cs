﻿using UnityEngine;
using System.Collections;

public class Enemys : MonoBehaviour
{
    GameObject g;

    private enemyResistance eResistance;
    private pauseManager pManager;
    private _Textures textures;

    public bool isDamage;
    bool canShot, canThrow;

    float dimension, second;
    public int id = 0, r, randomBehaviour, randomSkin, damage;

    void Start()
    {
        this.isDamage = false;
        this.canShot = false;
        this.canThrow = false;

        this.eResistance = enemyResistance.getInstance();
        this.pManager = pauseManager.getInstance();
        this.textures = _Textures.getInstance();

        this.StartCoroutine(this.anim(this.gameObject));
        this.StartCoroutine(this.randomB());

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<SoundFX>();
        this.gameObject.AddComponent<isDamage>();

        this.randomSkin = Random.Range(0, 2);

        this.gameObject.GetComponent<SpriteRenderer>().sprite = (this.randomSkin == 0) ? this.textures.enemys[this.id] : this.textures.enemys2[this.id];
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 5;

        this.randomBehaviour = Random.Range(0, 3);
        this.r = Random.Range(0, 2);

        this.transform.position = (this.r == 1) ? new Vector2(6, GameObject.FindObjectOfType<Player>().transform.position.y) : new Vector2(6, Random.Range(-0.4f, -2.5f));

        this.eResistance.setSkin(this.randomSkin);
    }

    void Update()
    {
        if (!this.pManager.getPause())
        {
            this.toDestroy(this.textures.enemys);
            this.toDestroy(this.textures.enemys2);

            this.dimension = -this.transform.position.y + this.textures.enemys[this.id].rect.height / 100 + 2;
            this.transform.localScale = new Vector2((this.dimension * 2) / 10, (this.dimension * 2) / 10);
        }

    }

    void toDestroy(Sprite[] x)
    {
        if ((this.transform.position.x + x[0].rect.width / 100) < -6)
            Destroy(this.gameObject);
    }

    void FixedUpdate()
    {
        this.addPolygon();
        if (!this.pManager.getPause())
            transform.Translate(-.054f, 0, 0);
    }

    void addPolygon()
    {
        if (!this.gameObject.GetComponent<PolygonCollider2D>())
            this.gameObject.AddComponent<PolygonCollider2D>();

        this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
    }

    IEnumerator anim(GameObject g)
    {
        while (true)
        {
            yield return new WaitForSeconds(.1f);

            if (!this.pManager.getPause())
            {
                //correndo
                if (!this.canShot && !this.canThrow && !this.isDamage)
                    this.id = (this.id >= 2) ? 0 : this.id += 1;

                //atirando
                if (this.canShot && !this.canThrow && !this.isDamage)
                {
                    if (this.gameObject.transform.position.y <= GameObject.FindObjectOfType<Player>().transform.position.y
                            && this.gameObject.transform.position.y >= GameObject.FindObjectOfType<Player>().transform.position.y - .5f
                            && this.gameObject.transform.position.x > GameObject.FindObjectOfType<Player>().transform.position.x)
                        this.id = (this.id >= 7) ? 0 : this.id += 1;
                    if (this.id >= 7)
                    {
                        g = new GameObject("b1", typeof(Shot));
                        g.gameObject.GetComponent<Shot>().position = this.gameObject.transform.position;
                        this.gameObject.GetComponent<SoundFX>().liberty = true;
                        this.canShot = false;
                    }
                }

                //arremessando 
                if (this.canThrow && !this.canShot && !this.isDamage)
                {
                    if (this.gameObject.transform.position.y <= GameObject.FindObjectOfType<Player>().transform.position.y
                            && this.gameObject.transform.position.y >= GameObject.FindObjectOfType<Player>().transform.position.y - .5f
                            && this.gameObject.transform.position.x > GameObject.FindObjectOfType<Player>().transform.position.x)
                    {
                        this.id = (this.id >= 9) ? 0 : this.id += 1;
                        if (this.id >= 9)
                        {
                            g = new GameObject("o0", typeof(Obstacles));
                            g.gameObject.GetComponent<Obstacles>().position = this.gameObject.transform.position;
                            this.canThrow = false;
                        }
                    }
                }

                //dano
                if (this.isDamage)
                {
                    this.id = (this.id >= 10) ? 0 : this.id += 1;
                    if (this.id >= 10)
                        this.isDamage = false;
                }

                this.gameObject.GetComponent<SpriteRenderer>().sprite = (this.randomSkin == 0) ? this.textures.enemys[this.id] : this.textures.enemys2[this.id];
            }
        }
    }

    IEnumerator randomB()
    {
        while (true)
        {
            yield return new WaitForSeconds(.5f);

            if (!this.pManager.getPause())
            {
                switch (this.randomBehaviour)
                {
                    case 2:
                        canShot = true;
                        break;
                    case 1:
                        yield return new WaitForSeconds(.5f);
                        canThrow = true;
                        if (this.gameObject.transform.position.y <= GameObject.FindObjectOfType<Player>().transform.position.y
                            && this.gameObject.transform.position.y >= GameObject.FindObjectOfType<Player>().transform.position.y - .5f
                            && this.gameObject.transform.position.x > GameObject.FindObjectOfType<Player>().transform.position.x)
                            this.id = 7;
                        break;
                }
            }
        }
    }
}
