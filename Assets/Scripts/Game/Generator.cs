﻿using UnityEngine;
using System.Collections;

public class Generator : MonoBehaviour {

    private pauseManager pManager;
    GameObject g;

	void Start () {
        this.pManager = pauseManager.getInstance();

        this.StartCoroutine(this.enemyGenerator(this.g));
        //this.StartCoroutine(this.obstacleGenerator(this.g));
	}

	void Update () {
	
	}

    IEnumerator enemyGenerator(GameObject g)
    {
        while(true)
        {
            yield return new WaitForSeconds(5);
            if(!this.pManager.getPause())
            g = new GameObject("enemy", typeof(Enemys));
        }
    }

    IEnumerator obstacleGenerator(GameObject g)
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            if (!this.pManager.getPause())
            g = new GameObject("obstacle", typeof(Obstacles));
        }
    }
}
