﻿using UnityEngine;
using System.Collections;

public class enemyResistance {
    private static enemyResistance instance = null;

    public static enemyResistance getInstance()
    {
        if (instance == null)
            instance = new enemyResistance();
        return instance;
    }

    private int resistance,skin;

    public enemyResistance()
    {
        this.resistance = 0;
    }

    public int getResistance(){
        switch(this.skin){
            case 0:
                return 2;
            case 1:
                return 3;
        }

        return 0;
    }

    public void setSkin(int s) { this.skin = s; }
}
