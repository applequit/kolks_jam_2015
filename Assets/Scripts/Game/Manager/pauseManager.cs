﻿using UnityEngine;
using System.Collections;

public class pauseManager {

    private static pauseManager instance;

    public static pauseManager getInstance()
    {
        if (instance == null)
            instance = new pauseManager();
        return instance;
    }

    private bool isPause;

    public pauseManager()
    {
        this.isPause = false;
    }

    public bool getPause() { return this.isPause; }
    public void setPause(bool pause) { this.isPause = pause; }
}
