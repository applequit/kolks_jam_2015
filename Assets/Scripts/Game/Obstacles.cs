﻿using UnityEngine;
using System.Collections;

public class Obstacles : MonoBehaviour
{
    GameObject g;

    private pauseManager pManager;
    private _Textures textures;

    public Vector2 position;

    bool canThrow;

    float dimension;
    int id = 0;

    void Start()
    {
        this.canThrow = true;

        this.pManager = pauseManager.getInstance();
        this.textures = _Textures.getInstance();

        this.StartCoroutine(this.anim());

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<Rigidbody2D>();

        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.textures.cocktail[this.id];
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 5;

        this.transform.position = (this.gameObject.name.Substring(1) == "0") ? this.position :
                                                                               new Vector2(6, Random.Range(-.5f, -2.5f));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!this.pManager.getPause())
        {
            while (this.canThrow)
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-300, 100));
                this.canThrow = false;
            }
        }

        this.addPolygon();
    }

    void Update()
    {
        if (!this.pManager.getPause())
        {
            if (this.transform.position.x + this.textures.cocktail[this.id].rect.width / 100 < -4 || this.transform.position.y + this.textures.cocktail[this.id].rect.height / 100 < -3)
                Destroy(this.gameObject);

            this.dimension = -this.transform.position.y + this.textures.cocktail[this.id].rect.height / 100 + 2;
            this.transform.localScale = new Vector2((this.dimension * 2) / 10, (this.dimension * 2) / 10);

            Time.timeScale = 1;
        }

        else
            Time.timeScale = 0;
    }

    void addPolygon()
    {
        if (!this.gameObject.GetComponent<PolygonCollider2D>())
            this.gameObject.AddComponent<PolygonCollider2D>();

        this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.name.Equals("player"))
        {
            Destroy(this.gameObject);
            c.gameObject.GetComponent<Player>().triggerFire = true;
        }
    }

    IEnumerator anim()
    {
        while (true)
        {
            yield return new WaitForSeconds(.1f);
            if (!this.pManager.getPause())
            {
                //coquetel molotov
                this.id = (this.id >= 1) ? 0 : this.id += 1;
                this.gameObject.GetComponent<SpriteRenderer>().sprite = this.textures.cocktail[this.id];
            }
        }
    }
}
