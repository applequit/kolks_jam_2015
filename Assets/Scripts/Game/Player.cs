﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    GameObject g;

    private Score score;
    private pauseManager pManager;
    private _Textures textures;

    int id = 0;
    public int damage, ammo;

    float scale, dimension, countFire;

    bool canShot, catchingFire, ownDamage, isDamage;
    public bool triggerFire, triggerDamage;

    void Awake()
    {
    }

    void Start()
    {
        this.ammo = 5;

        this.score = Score.getInstance();
        this.pManager = pauseManager.getInstance();
        this.textures = _Textures.getInstance();

        this.isDamage = false;
        this.triggerFire = false;
        this.canShot = false;
        this.catchingFire = false;
        this.ownDamage = false;

        this.score.setScore(0);

        this.StartCoroutine(this.anim());

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<SoundFX>();
        this.gameObject.AddComponent<isDamage>();

        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.textures.player[this.id];
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 6;

        this.transform.position = new Vector2(-3, -0.4f);
    }

    void Update()
    {
        if (!this.pManager.getPause())
        {
            if (Input.GetKey(KeyCode.Space))
                this.canShot = true;

            while (this.triggerFire)
            {
                this.catchingFire = true;
                this.countFire = 0;
                this.id = 7;

                this.gameObject.GetComponent<SoundFX>().liberty = true;
                this.gameObject.GetComponent<SoundFX>().sublabel = "inflame";

                this.triggerFire = false;
            }

            while (this.triggerDamage)
            {
                this.ownDamage = true;
                this.id = 10;

                this.triggerDamage = false;
            }

            this.dimension = -this.transform.position.y + this.textures.player[this.id].rect.height / 100 + 2;
            this.transform.localScale = new Vector2((this.dimension * 2) / 10, (this.dimension * 2) / 10);
        }
    }

    void FixedUpdate()
    {
        this.addPolygon();
        if (!this.pManager.getPause())
        {
            if (this.transform.position.y + this.textures.player[0].rect.height / 100 < .5f)
                if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                    this.transform.Translate(0, .1f, 0);

            if (this.transform.position.y + this.textures.player[0].rect.height / 100 > -1.2f)
                if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                    this.transform.Translate(0, -.1f, 0);

            if (this.isDamage)
            {
                transform.Translate(-.054f, 0, 0);
                this.id = 10;
                if (this.transform.position.x < -5f)
                    Application.LoadLevel("Score");
            }
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.name == "b1" || c.name == "o0")
            this.damage++;

        if (c.name == "ammo")
            this.gameObject.GetComponent<SoundFX>().sublabel = "reload";

        if (c.name == "water")
            this.score.num += 50;

        if (this.damage >= 3)
            this.isDamage = true;
    }

    void addPolygon()
    {
        if (!this.gameObject.GetComponent<PolygonCollider2D>())
            this.gameObject.AddComponent<PolygonCollider2D>();

        this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
    }

    IEnumerator anim()
    {
        while (true)
        {
            yield return new WaitForSeconds(.08f);

            if (!this.pManager.getPause())
            {
                //correndo normal
                if (!this.canShot && !this.catchingFire && !this.ownDamage)
                    this.id = (this.id >= 2) ? this.id = 0 : this.id += 1;

                //atirando
                if (this.canShot && !this.catchingFire && !this.ownDamage)
                {
                    this.id = (this.id >= 7) ? 0 : this.id += 1;
                    if (this.id >= 7)
                    {
                        this.g = new GameObject("b0", typeof(Shot));
                        this.gameObject.GetComponent<SoundFX>().liberty = true;
                        this.gameObject.GetComponent<SoundFX>().sublabel = "empty";
                        this.canShot = false;
                    }
                }

                //pegando fogo
                if (this.catchingFire && !this.ownDamage)
                {
                    this.id = (this.id >= 9) ? 8 : this.id += 1;
                    this.countFire += .1f;
                    if (this.countFire >= 3)
                        this.catchingFire = false;
                }

                //dano
                if (this.ownDamage)
                {
                    this.id++;
                    if (this.id >= 11)
                        this.ownDamage = false;
                }


                this.gameObject.GetComponent<SpriteRenderer>().sprite = this.textures.player[this.id];
            }
        }
    }
}

