﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour
{
    private Score score;
    private enemyResistance eResistance;
    private pauseManager pManager;
    private _Textures textures;

    int id = 0;
    float dimension;

    GameObject g;

    public Vector2 position;

    void Start()
    {
        this.score = Score.getInstance();
        this.eResistance = enemyResistance.getInstance();
        this.pManager = pauseManager.getInstance();
        this.textures = _Textures.getInstance();

        this.StartCoroutine(this.anim());

        this.gameObject.AddComponent<SpriteRenderer>();
        this.gameObject.AddComponent<Rigidbody2D>();

        this.GetComponent<Rigidbody2D>().fixedAngle = true;
        this.GetComponent<Rigidbody2D>().gravityScale = 0;

        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.textures.shot[this.id];
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 5;

        this.transform.position = new Vector3(this.gameObject.name.Substring(1) == "0" ? GameObject.FindObjectOfType<Player>().transform.position.x + 1 : this.position.x,
            this.gameObject.name.Substring(1) == "0" ? GameObject.FindObjectOfType<Player>().transform.position.y + .1f : this.position.y + .2f, 0);
    }

    void Update()
    {
        if (!this.pManager.getPause())
        {
            if ((this.transform.position.x + this.textures.shot[this.id].rect.width / 100) < -4.5f || (this.transform.position.x + this.textures.shot[this.id].rect.width / 100) > 4.5f)
                Destroy(this.gameObject);

            this.dimension = -this.transform.position.y + this.textures.shot[this.id].rect.height / 100 + 2;
            this.transform.localScale = new Vector2((this.dimension * 2) / 10, (this.dimension * 2) / 10);
        }
    }

    void FixedUpdate()
    {
        this.addPolygon();
        if (!this.pManager.getPause())
            this.transform.Translate(this.gameObject.name.Substring(1) == "0" ? .2f : -.2f, 0, 0);
    }

    void addPolygon()
    {
        if (!this.gameObject.GetComponent<PolygonCollider2D>())
            this.gameObject.AddComponent<PolygonCollider2D>();

        this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.name.Equals("enemy") && this.gameObject.name.Substring(1) == "0")
        {
            Destroy(this.gameObject);

            c.gameObject.GetComponent<Enemys>().isDamage = true;
            c.gameObject.GetComponent<Enemys>().id = 9;

            c.gameObject.GetComponent<isDamage>().GetComponent<Animator>().SetBool("damageOn", true);

            c.gameObject.GetComponent<Enemys>().damage++;

            if (c.gameObject.GetComponent<Enemys>().damage >= this.eResistance.getResistance())
            {
                if (c.gameObject.GetComponent<Enemys>().randomSkin == 1)
                {
                    g = new GameObject("water", typeof(Collectible));
                    g.gameObject.GetComponent<Collectible>().skin = 0;
                    g.gameObject.GetComponent<Collectible>().position = c.gameObject.GetComponent<Enemys>().transform.position;
                }

                else if (c.gameObject.GetComponent<Enemys>().randomSkin == 0)
                {
                    g = new GameObject("ammo", typeof(Collectible));
                    g.gameObject.GetComponent<Collectible>().skin = 1;
                    g.gameObject.GetComponent<Collectible>().position = c.gameObject.GetComponent<Enemys>().transform.position;
                }

                this.score.num += 5;
                Destroy(c.gameObject);
            }
        }

        else if (c.name.Equals("player") && this.gameObject.name.Substring(1) == "1")
        {
            Destroy(this.gameObject);

            c.gameObject.GetComponent<isDamage>().GetComponent<Animator>().SetBool("damageOn", true);
            c.gameObject.GetComponent<Player>().triggerDamage = true;
        }

        if (c.name.StartsWith("b") || c.name.StartsWith("o"))
        {
            Destroy(c.gameObject);
            Destroy(this.gameObject);
        }

        print(": " + c.name);
    }

    IEnumerator anim()
    {
        while (true)
        {
            yield return new WaitForSeconds(.1f);

            if (!this.pManager.getPause())
            {
                this.id = (this.id >= 3) ? 0 : this.id += 1;
                this.gameObject.GetComponent<SpriteRenderer>().sprite = this.textures.shot[this.id];
            }
        }
    }
}
