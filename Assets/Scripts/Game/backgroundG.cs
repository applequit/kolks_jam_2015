﻿using UnityEngine;
using System.Collections;

public class backgroundG : MonoBehaviour
{
    private _Textures textures;
    private pauseManager pManager;

    private Vector3[] positions = new Vector3[]{
        new Vector3(0,1.5f,0),
        new Vector3(0,1.53f,0),
        new Vector3(0, .9f,0),
        new Vector3(0,0,0),
        new Vector3(0,-1.7f,0)
    };

    void Start()
    {
        this.pManager = pauseManager.getInstance();
        this.textures = _Textures.getInstance();

        this.gameObject.AddComponent<SpriteRenderer>();

        this.gameObject.GetComponent<SpriteRenderer>().sprite = this.textures.backgroundGame[int.Parse(this.gameObject.name.Substring(1))];
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = int.Parse(this.gameObject.name.Substring(1));
        
        if (this.gameObject.name.StartsWith("a"))
            this.gameObject.transform.position = this.positions[int.Parse(this.gameObject.name.Substring(1))];
        else
        {
            this.gameObject.transform.position = new Vector3(GameObject.Find("a" + int.Parse(this.gameObject.name.Substring(1))).transform.position.x + (this.textures.backgroundGame[int.Parse(this.gameObject.name.Substring(1))].rect.width / 101.5f), this.positions[int.Parse(this.gameObject.name.Substring(1))].y, 0);
            this.transform.localScale = new Vector2(this.gameObject.name.StartsWith("a") ? GameObject.Find("b" + int.Parse(this.gameObject.name.Substring(1))).transform.localScale.x * -1 : GameObject.Find("a" + int.Parse(this.gameObject.name.Substring(1))).transform.localScale.x * -1, 1);

        }
    }

    void FixedUpdate()
    {
        if ((this.transform.position.x + (this.textures.backgroundGame[int.Parse(this.gameObject.name.Substring(1))].rect.width / 100)) <= 0)
        {
            this.transform.localScale = new Vector2(this.gameObject.name.StartsWith("a") ? GameObject.Find("b" + int.Parse(this.gameObject.name.Substring(1))).transform.localScale.x * -1 : GameObject.Find("a" + int.Parse(this.gameObject.name.Substring(1))).transform.localScale.x * -1, 1);
            this.gameObject.transform.position = (this.transform.gameObject.name.StartsWith("a")) ? new Vector3(GameObject.Find("b" + int.Parse(this.gameObject.name.Substring(1))).transform.position.x + (this.textures.backgroundGame[int.Parse(this.gameObject.name.Substring(1))].rect.width / 101.5f), this.positions[int.Parse(this.gameObject.name.Substring(1))].y, 0) :
                                                                                                   new Vector3(GameObject.Find("a" + int.Parse(this.gameObject.name.Substring(1))).transform.position.x + (this.textures.backgroundGame[int.Parse(this.gameObject.name.Substring(1))].rect.width / 101.5f), this.positions[int.Parse(this.gameObject.name.Substring(1))].y, 0);
        }

        if (!this.pManager.getPause())
        {
            //velocidade
            switch (this.gameObject.name.Substring(1))
            {
                case "1":
                    transform.Translate(-.038f, 0, 0);
                    break;
                case "2":
                    transform.Translate(-.048f, 0, 0);
                    break;
                case "3":
                    transform.Translate(-.058f, 0, 0);
                    break;
                case "4":
                    transform.Translate(-.064f, 0, 0);
                    break;
            }
        }
    }
}
