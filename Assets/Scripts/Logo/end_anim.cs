﻿using UnityEngine;
using System.Collections;

public class end_anim : MonoBehaviour
{
    public bool end;
    void Start()
    {
        this.end = false;
    }
    void Update()
    {

        switch (Application.loadedLevelName)
        {
            case "Logo":
                if (this.end) Application.LoadLevel("Splash");
                break;
            case "Splash":
                if (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.Escape)) Application.LoadLevel("Info");
                break;
            case "Info":
                if (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.Escape))
                    Application.LoadLevel("Game");
                break;
            case "Score":
                if (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.Escape)) Application.LoadLevel("Splash");
                break;
        }
    }
}
