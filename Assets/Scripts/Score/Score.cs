﻿using UnityEngine;
using System.Collections;

public class Score{

    private static Score instance = null;

    public static Score getInstance()
    {
        if (instance == null)
            instance = new Score();
        return instance;
    }

    public int num;

    public Score()
    {
        this.num = 0;
    }

    public void setScore(int i) { this.num = i; }
    public int getScore() { return this.num; }
}
