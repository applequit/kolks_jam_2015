﻿using UnityEngine;
using System.Collections;

public class ScoreInterface : MonoBehaviour
{
    private Score _score;
    GUIStyle score;

    void Start()
    {
        this._score = Score.getInstance();
        this.score = new GUIStyle();

        this.score.fontSize = (int)(Screen.height*.1f);
        this.score.normal.textColor = Color.white;
        this.score.alignment = TextAnchor.MiddleCenter;

    }

    void Update()
    {

    }

    void OnGUI()
    {
        GUI.Label(new Rect(Screen.width*.5f, Screen.height*.5f,0,0), "Score: " + this._score.getScore(), this.score);
    }
}
