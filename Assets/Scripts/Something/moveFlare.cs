﻿using UnityEngine;
using System.Collections;

public class moveFlare : MonoBehaviour {

    private pauseManager pManager;

    float speed, intensity;

    void Start()
    {
        this.pManager = pauseManager.getInstance();
    }
	
	void FixedUpdate () {
        if (!this.pManager.getPause())
        {
            this.speed = -(Time.deltaTime * .1f);

            if (this.transform.position.x <= -5f)
                this.transform.position = new Vector3(5f, this.transform.position.y, this.transform.position.z);

            this.transform.Translate(this.speed, 0, 0);
        }
	}
}
