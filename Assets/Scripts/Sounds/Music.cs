﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour
{
    private pauseManager pManager;

    AudioClip music;

    bool mute;

    void Start()
    {
        this.pManager = pauseManager.getInstance();

        this.mute = false;
        this.music = (AudioClip)Resources.Load("Sounds/Music/background");

        this.gameObject.AddComponent<AudioSource>();
        this.gameObject.GetComponent<AudioSource>().clip = this.music;
        this.gameObject.GetComponent<AudioSource>().Play();
        this.gameObject.GetComponent<AudioSource>().loop = true;
    }

    void Update()
    {

        if (Input.GetKeyUp(KeyCode.M) && !this.mute)
            this.mute = true;
        else if (Input.GetKeyUp(KeyCode.M) && this.mute)
            this.mute = false;


        if (Input.GetKeyUp(KeyCode.M))
            if (!this.mute) this.gameObject.GetComponent<AudioSource>().Play(); else this.gameObject.GetComponent<AudioSource>().Pause();

        if (pManager.getPause())
            this.gameObject.GetComponent<AudioSource>().volume = .1f;
        else
            this.gameObject.GetComponent<AudioSource>().volume = 1;
    }
}
