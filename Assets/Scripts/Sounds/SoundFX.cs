﻿using UnityEngine;
using System.Collections;

public class SoundFX : MonoBehaviour
{
    AudioClip[] sound;

    public string label, sublabel;
    public bool liberty;

    void Awake()
    {
        this.liberty = false;

        this.sound = new AudioClip[3];

        this.label = this.gameObject.name;

        this.gameObject.AddComponent<AudioSource>();
    }

    void Update()
    {
        while (this.liberty)
        {
            switch (this.label)
            {
                case "player":
                    if (this.sublabel == "inflame")
                        this.LoadSound(2);
                    else if (this.sublabel == "empty" && this.sublabel != "reload")
                        this.LoadSound(0);
                    else if (this.sublabel == "reload")
                        this.LoadSound(1);
                    break;
                case "enemy":
                    this.LoadSound(0);
                    break;
            }
            this.liberty = false;
        }
    }

    void LoadSound(int id)
    {
        this.sound[0] = Resources.Load("Sounds/FX/" + "shooting") as AudioClip;
        this.sound[1] = Resources.Load("Sounds/FX/" + "reload") as AudioClip;
        this.sound[2] = Resources.Load("Sounds/FX/" + "inflames") as AudioClip;

        this.gameObject.GetComponent<AudioSource>().clip = this.sound[id];
        this.gameObject.GetComponent<AudioSource>().Play();

    }
}
